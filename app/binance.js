/**
 * External Imports
 */
const BnbApiClient = require('@binance-chain/javascript-sdk');
const WebSocket = require('ws');

/**
 * Local Imports
 */
const config = require('../config/config').config;
const utils = require('./utils');
const Users = require('./users'); 
const telegram = require('./telegram');
const wallet = require('./wallet');


/**
 * Start Binance Chain Client
 */
const bnbClient = new BnbApiClient(config.CHAIN_APIURI);
bnbClient.chooseNetwork("testnet");
bnbClient.initChain()
bnbClient.setPrivateKey(config.CHAIN_PRIVKEY).then(success => utils.winston.info("PRIVATE KEY SET"));


/**
 * Returns balance for given address from binance chain
 * @param  {String} address     The BEP2 address.
 * @return {Object}             Object of balances
 */
function getBalance(address) {
    return new Promise((resolve, reject) => {
        bnbClient.getBalance(address)
        .then(balance => {resolve(balance)})
        .catch(err => reject(new Error(err)));
    });
}

/**
 * Handles binance multisend transaction
 * @param  {String} userId     The telegram userid.
 * @param  {Object} outputs    An object wtih the binance multisend outputs.
 * @param  {String} memo       Binance chain memo for the tx.
 * @return {Object}             Object of balances
 */
function multiSend(userId, outputs, memo){
return new Promise((resolve, reject) => {
    bnbClient.multiSend(config.CHAIN_ADDRESS, outputs, memo)
        .then(resp => {
            utils.winston.info("CHAIN: TOKENS SENT "+resp.result[0].hash+" USER "+userId);
            resolve(resp.result[0].hash);
        })
        .catch(err => {
            utils.winston.warn("TX FAILED FOR "+userId)
            reject(err)
        })
});
}


/**
 * Processes the payload received from the Binance websocket
 * @param  {Object} payload     The websocket message payload
 */
function checkPayload(payload){
    var streamType = payload.stream;
  
    if(!streamType){
        utils.winston.warn("DETECTED PAYLOAD WITHOUT STREAM");
        return false;
    } else {
        switch(streamType){
            case "accounts":
                break;
  
            case "transfers":
                let tx = utils.parseTransaction(payload)

                // Process incoming transactions
                if(tx.txTo !== config.CHAIN_ADDRESS) {
                    utils.winston.info("CHAIN: SENT "+tx.txQty+" "+tx.txTkn+" TOKENS TO "+tx.txTo);
                } else {
                    utils.winston.info("CHAIN: RECEIVED "+tx.txQty+" "+tx.txTkn+" TOKENS FROM "+tx.txFrom);    
                    let userInfo = utils.getUserByAddress(tx.txFrom);
                    if(!userInfo || !userInfo.userid){
                        utils.winston.info("CHAIN: RECEIVED "+tx.txQty+" "+tx.txTkn+" TOKENS FROM UNKNOWN ADDRESS "+tx.txFrom);  
                    } else {
                        Promise.all([
                            Users.updateBalance(userInfo.userid,tx.txQty, tx.txTkn),
                            wallet.updateLedger(userInfo, tx.txQty, tx.txTkn, tx.txHash)
                        ])
                        .then(userData => {
                            let outMsg = utils.format(config.strings.depositConf,tx.txTkn, tx.txQty, utils.shortHash(tx.txHash),tx.txHash);
                            Users.messageUser(userInfo.userid,outMsg,telegram.depositMenu)
                            utils.winston.info("CHAIN: CREDITED "+tx.txQty+" "+tx.txTkn+" TO @"+userInfo.username); 
                            return true;
                        }) 
                        .catch(error => {
                            utils.winston.warn("CHAIN: FAILED TO UPDATE BALANCES/LEDGER FOR TX HASH:"+ tx.txHash);
                            /** @todo refund transaction & unpick updates or retry? */ 
                            return false;
                        })
                    }
                }
            break;
  
        default:
            // Unknown stream type
            utils.winston.warn("CHAIN: UNKNOWN STREAM TYPE");
        }
    }
}


/**
 * Binance Chain Websocket events
 */
function connectWebsocket(){
    var ws = new WebSocket(config.CHAIN_WSURI+config.CHAIN_ADDRESS);    
    
    ws.on('open', function open() {
        utils.winston.info('WEBSOCKET CONNECTED');
    });

    ws.on('message', function incoming(data) {
        let payload = JSON.parse(data);
        checkPayload(payload);
    });

    ws.on('error', function error(e) {
        utils.winston.warn('WEBSOCKET ERRORED: '+e);
    });

    ws.on('close', function close(e) {
        utils.winston.warn('WEBSOCKET CLOSED WITH ERROR: '+e);
        wsReconnect()
    });

    ws.on('ping', function ping(data) {
        utils.winston.silly('WEBSOCKET PING RECEIVED');
        ws.isAlive = true;
    });
}

function wsReconnect(wsRecon){
        setTimeout(x => {
            return connectWebsocket()
        },1000)
}


//connectWebsocket();


/**
 * EXPORTS
 */
exports.bnbClient = bnbClient
exports.getBalance = getBalance
exports.multiSend = multiSend