/**
 * Local Imports
 */
const config = require('../config/config').config;
const mongo = require('./mongo');
const utils = require('./utils');
const telegram = require('./telegram')
const binance = require('./binance')


/**
 * Adds a new telegram user to the bot
 * @param  {Object} userObj     The telegram user object
 * @return {Object}             User object (id, username, balance etc)
 */
function addUser(userObj){
    return new Promise((resolve, reject) => {
        var myDate = new Date();

        var userData = {
            datejoined:myDate,
            userid:userObj.id,
            username:userObj.username ? userObj.username : "unknown",
            bepaddress:"",
            faucetTx:""
        }

        mongo.dbAddUser(userData)
            .then(newUser => {
                config.users.push(newUser)
                resolve(newUser)
            })
            .catch(err => {
                let error = new Error("DB_FAIL")
                reject(error);
            })
    });
}


/**
 * Checks if user is existing, otherwise adds a new user
 * @param  {Object} userObj         The telegram user object
 * @return {Object}                 User object (id, username, balance etc)
 */
function addUserIfNew(userObj){
    try {
        utils.isUser(userObj) 
    } catch (error) {
        addUser(userObj)
    }
}


/**
 * Adds/Updates the users bepaddress
 * @param  {Object} messageObj      The telegram message object
 * @return {Object}                 User object (id, username, balance etc)
 */
function updateAddress(messageObj, givenAddress, callback){
    let userInfo = utils.grabUser(messageObj.from)
    let bepAddress = false;

    if(!givenAddress){
        let splitMsg = messageObj.text ? utils.splitMsg(messageObj.text) : false
        bepAddress = splitMsg && splitMsg.args[0] ? splitMsg.args[0] : false
    } else {
        bepAddress = givenAddress
    }

    if(!userInfo){
        let error = new Error("USER_NOTREG")
        let outMsg = utils.sexyError(error.message);
        return callback(outMsg);
    }

    // Return if no address provided
    if(!bepAddress) {
        let error = new Error("ADDR_NOADDR")
        let outMsg = utils.sexyError(error.message);
        return callback(outMsg);
    }

    // Check if valid address
    let validAddress = binance.bnbClient.checkAddress(bepAddress);

    if(!validAddress) {
        let error = new Error("ADDR_NOTVALID")
        let outMsg = utils.sexyError(error.message);
        return callback(outMsg);
    }

    // Check if in use
    let users = config.users.filter(x => x.bepaddress == bepAddress)
    if(users[0]){
        let error = new Error("ADDR_INUSE")
        let outMsg = utils.sexyError(error.message);
        return callback(outMsg);
    }

    // Update db
    mongo.dbUpdateAddress(userInfo.userid, bepAddress)
        .then(userData => {
            config.users = config.users.filter(x => x.userid != userInfo.userid);
            config.users.push(userData)
            userData.ok = true;
            return callback("💰 Thanks! Your address has been updated.\n\nYou can now get tokens with `/gettokens` or use the keyboard menu.")
        })
        .catch(error => {
            let outMsg = utils.sexyError(error.message);
            return callback(outMsg)
        })
}


/**
 * Sends a message proactively to a known user
 * @param  {Object} userId        Telegram userid
 */
function messageUser(userid, outMsg){
    return telegram.bot.telegram.sendMessage(userid, outMsg, telegram.Extra.markup(telegram.clearButton) .webPreview(false) .markdown(true))
}

/**
 * EXPORTS
 */
exports.addUserIfNew = addUserIfNew;
exports.addUser = addUser;
exports.updateAddress = updateAddress;
exports.messageUser = messageUser