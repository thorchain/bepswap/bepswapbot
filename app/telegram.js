/**
 * External Imports
 */
const Telegraf = require('telegraf');
const Markup = require('telegraf/markup');
const Extra = require('telegraf/extra');


/**
 * Local Imports
 */
const utils = require('./utils');
const Users = require('./users');
const wallet = require('./wallet');
const config = require('../config/config').config;
const session = require('telegraf/session');


/**
 * Constants
 */
const bot = new Telegraf(config.TELEGRAM_TOKEN);
bot.use(session());

/**
 * Telegram bot error handling
 */
bot.catch((err) => {
    console.warn('TELEGRAM ERR: ', err);
});


/**
 * Telegram inline menus
 */
const clearButton = Markup.inlineKeyboard([
  Markup.callbackButton('✅ Clear', 'delete')
]);

const inlineMenu = Markup.inlineKeyboard([
  [Markup.callbackButton('⚠️ Notice', 'deposit-notice'),
  Markup.callbackButton('✅ Instructions', 'deposit-instruction')],
 [Markup.callbackButton('📮 Address & QR Code', 'depositaddress'),
  Markup.callbackButton('✅ Clear Message', 'delete')]
]);


/**
 * Telegram Bot Listeners
 */

bot.action('delete', ({ deleteMessage }) => {
  deleteMessage()
  .then()
  .catch(err => {console.log(err);});
});

/**
 * Telegram Bot Listeners
 */
bot.command('start', ({ replyWithMarkdown }) => {
    return replyWithMarkdown(config.strings.welcome, Markup.keyboard(config.strings.start) .oneTime() .resize() .extra());
});

bot.hears(['about','⚡️ Bot Info'], ctx => {
  Users.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(config.strings.about,Extra.markup(clearButton) .webPreview(false));
});

bot.command(['addwallet'], ctx => {
  Users.addUserIfNew(ctx.from)
  Users.updateAddress(ctx.message, null, outMsg => {
    ctx.replyWithMarkdown(outMsg);
  });
});

bot.hears([/^tbnb/], ctx => {
  Users.addUserIfNew(ctx.from)
  Users.updateAddress(ctx.message, ctx.message.text, outMsg => {
    ctx.replyWithMarkdown(outMsg);
  });
});

bot.hears(['💳 Add Wallet'], ctx => {
  Users.addUserIfNew(ctx.from)
  Users.updateAddress(ctx.message, null, outMsg => {
    ctx.replyWithMarkdown(outMsg);
  });
});

bot.command(['getwallet'], ctx => {
  Users.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(utils.format(config.strings.wdrl_pending), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false))
  .then(res => {
    wallet.withdrawTokens(ctx.message, outMsg => {
      bot.telegram.editMessageText(res.chat.id,res.message_id,null,outMsg,Extra.markup(clearButton) .markdown(true) .webPreview(false));
    });
  });
});

bot.hears(['💰 Get Wallet'], ctx => {
  Users.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(utils.format(config.strings.wdrl_pending), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false))
  .then(res => {
    wallet.withdrawTokens(ctx.message, outMsg => {
      bot.telegram.editMessageText(res.chat.id,res.message_id,null,outMsg,Extra.markup(clearButton) .markdown(true) .webPreview(false));
    });
  });
});

/**
 * EXPORTS
 */
exports.bot = bot;
exports.Extra = Extra;
exports.clearButton = clearButton
