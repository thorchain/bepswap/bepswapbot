/**
 * Local Imports
 */
const config = require('../config/config').config;
const format = require('string-format');
const winston = require('winston');

winston.add(new winston.transports.File({ filename: 'logfile.log' }))
winston.add(new winston.transports.Console())

var paginationText = {};
paginationText.tips = "tips";
paginationText.transactions = "transactions";

const errorList = {
  INVALID_COMMAND: "📆 Whoops invalid command",
  ADDR_NOADDR:"You'll need to specify a testnet binance chain address. \n\nSyntax: `/addwallet <tbnb address>` (or paste your address in a message)",
  ADDR_NOTVALID:"You need to provide a valid testnet binance chain address.  \n\nSyntax: `/addwallet <tbnb address>`  (or paste your address in a message)",
  ADDR_INUSE:"You can't share an address with someone else.",
  USER_NOTREG:"You need to be registered for that. Start the bot `/start` in a private message to register.",
  ERR_UNKNOWN:"I has the dumb! Some unknown error happen daddy!",
  DB_FAIL: "There was an issue updating the database. Try again later",
  DB_INSERT_FAIL: "There was an issue updating the database. Try again later",
  WITHDRAW_BADINSTRUCTION:"You didn't provide the right instructions. You need to provide the token and amount you want to withdraw.",
  WITHDRAW_NOADDR:"You need to add a binance chain address before you can get tokens. Type `/addwallet <youraddress>` (or paste your address in a message).",
  WITHDRAW_GREEDY:"You've already received testnet tokens to your address. Please contact us if you need more.",
  WALLET_FAIL:"Unable to create wallet at this time. Please try again later."
}

format.extend(String.prototype, {})

 /**
 * Returns users chat text into an object split by commands & params
 * @param   {Object} msg    Telegram Message Object
 * @return  {Object}        Object containing text, command & arguments
 */
  function splitMsg(msg){

    const text = msg
    if (text.startsWith('/')) {
      const match = text.match(/^\/([^\s]+)\s?(.+)?/)
      let args = []
      let command
      if (match !== null) {
        if (match[1]) {
          command = match[1]
        }
        if (match[2]) {
          args = match[2].split(' ')
        }
      }

      let msgObj = {
        raw: text,
        command,
        args
      }

      return msgObj || false;
    }
  }


/**
 * Takes a binance chain payload & returns a neat object.
* @param   {Object}   payload    Binance payload object
* @return  {Object}              Neat object
*/
function parseTransaction(payload){
  let tx = {}
  tx.txHash   = payload.data.H;
  tx.txTo     = payload.data.t[0].o;
  tx.txFrom   = payload.data.f;
  tx.txTkn    = payload.data.t[0].c[0].a;
  tx.txQty    = parseInt(payload.data.t[0].c[0].A);
  return tx;
}

/**
 * Returns the bots user data object from a telegram user object
 * @param   {Object}  userObj    Telegram user object (or username)
 * @return  {Object}             Our user data object
 */
const grabUser = (userObj) => {
  if(userObj && userObj.id) { userObj.userid = userObj.id}
  var searchById = userObj.hasOwnProperty("userid") ? true : false;
  let userInfo = config.users.filter(x => searchById ? x.userid == userObj.userid : x.username == userObj.substring(1,userObj.length));
  return userInfo[0] ? userInfo[0] : false
}


/**
 * Other minor helper functions. Self explanatory
 */
const updateUserArray = (userInfo) => {
  config.users = config.users.filter(x => x.userid != userInfo.userid);
  config.users.push(userInfo)
 }

const numberWithCommas = (num) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const getChart = (num) => {
  return num < 0 ? '📉' : '📈';
}

const shortHash = (txHash) => {
  let newHash = txHash.substring(0,6)+txHash.substring(txHash.length - 6, txHash.length)
  return newHash;
}

const getUserByAddress = (chainAddress) => {
  return config.users.filter(x => x.bepaddress == chainAddress)[0] || new Error("USER_UNKNOWN")
}

const tokenToTicker = (token) => {
  return config.botTokens.filter(x => x.token == token).map(x => x.ticker)[0]
}

const tickerToToken = (ticker) => {
  return  config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x => x.token)[0] || makeError("TOKEN_INVALID");
}

const getTokenBalance  = (userData, token) => {
  return userData.balances.filter(x => x.token == token).map(x => x.balance)[0]
}

const isNumber = (number) => {
  var numCheck = new RegExp(/^\d+$/)
  return numCheck.test(number) || makeError("AMOUNT_INVALID")
}

const isAboveZero = (number) => {
  var numCheck = new RegExp(/^\d+$/)
  return numCheck.test(number) && number > 0 ? true : makeError("AMOUNT_INVALID")
}

const makeError = (message) => {
  throw new Error(message);
}

const hasBalance = (userData, token, amount) => {
  return getTokenBalance(grabUser(userData),token)-amount >=0 ? true : makeError("BALANCE_INSUFFICIENT")
}

const hasAddress = (userData) => {
  return userData.bepaddress ? true : makeError("WITHDRAW_NOADDR")
}

const isUser = (userObj) => {
  return grabUser(userObj) ? true : makeError("USER_UNKNOWN")
}

const isValidToken = (ticker) => {
  return ticker
  ? config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x => x.token)[0] ? true :  makeError("TOKEN_INVALID")
  : makeError("TOKEN_INVALID")
}

const sexyError = (error) => {
  return errorList[error];
}

const sexyDate = (ISODate) => {
  return ISODate.toISOString().replace(/T/, ' ').replace(/\..+/, '')
}

const hasWithdrawn = (userInfo) => {
  return userInfo.withdrawalTx ? makeError("WITHDRAW_GREEDY")  : false
}

const prettyBalances = (userInfo) => {
  let userBalances = "";
  userInfo.balances.forEach(e => {
      userBalances = userBalances + "▪ *"+tokenToTicker(e.token) + ":* " + e.balance + "\n"
  });
  return userBalances
}

/**
 * EXPORTS
 */
exports.prettyBalances = prettyBalances
exports.sexyDate = sexyDate
exports.getTokenBalance = getTokenBalance
exports.isNumber = isNumber
exports.hasBalance = hasBalance
exports.isUser = isUser
exports.grabUser = grabUser
exports.isAboveZero = isAboveZero
exports.winston = winston
exports.splitMsg = splitMsg
exports.numberWithCommas = numberWithCommas
exports.sexyError = sexyError;
exports.format = format;
exports.getChart = getChart;
exports.shortHash = shortHash
exports.tokenToTicker = tokenToTicker
exports.updateUserArray = updateUserArray
exports.getUserByAddress = getUserByAddress
exports.parseTransaction = parseTransaction
exports.hasAddress = hasAddress
exports.isValidToken = isValidToken
exports.tickerToToken = tickerToToken
exports.paginationText = paginationText
exports.hasWithdrawn = hasWithdrawn
