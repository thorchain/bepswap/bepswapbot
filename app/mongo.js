/**
 * External Imports
 */
const MongoClient = require('mongodb').MongoClient;


/**
 * Local Imports
 */
const utils = require('./utils');
const config = require('../config/config').config;
   

/**
 * Returns a collection from db
 * @param  {String} collectionName  The name of the collection.
 * @return {Object}                 collection from db
 */
function loadCollection(collectionName) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;
            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection(collectionName);
            collection.find({}).toArray()
                .then(response => resolve(response))
                .catch(err => reject(new Error(err)))
        });
    });
}

/**
 * Adds a user to the database
 * @param  {Object} userData    The new user object (@id , @username etc.)
 * @return {Object}               collection from db
 */
function dbAddUser(userData) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('users');

            collection.insertOne(userData)
                .then(response => resolve(userData))
                .catch(err => {
                    let error = new Error("DB_INSERT_FAIL")
                    reject(error)
                })
        })
    });
}


/**
 * Updates a BEP address
 * @param  {Number} userId          The telegram user Id
 * @param  {String} bepAddress      The address to be added
 * @return {Object}                 The resultant user object
 */
function dbUpdateAddress(userId, bepAddress) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('users');

            collection.findOneAndUpdate({userid: userId}, {$set: {bepaddress: bepAddress}}, {returnOriginal: false})
                .then(response => resolve(response.value))
                .catch(err => {
                    let error = new Error("DB_INSERT_FAIL")
                    utils.winston.warn("ADDRESS: COULDN'T UPDATE USERS ADDRESS")
                    reject(error)
                })
        })
    });
}


/**
 * Updates a withdrawalTx
 * @param  {Number} userId          The telegram user Id
 * @param  {String} txHash          The tx hash of the tbnb tx
 */
function dbAddWithdrawalTx(userId, txHash) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('users');

            collection.findOneAndUpdate({userid: userId}, {$set: {withdrawalTx: txHash}}, {returnOriginal: false})
                .then(response => resolve(response.value))
                .catch(err => {
                    let error = new Error("DB_INSERT_FAIL")
                    utils.winston.warn("TXHASH: COULDN'T UPDATE USER WITH HASH")
                    reject(error)
                })
        })
    });
}

/**
 * EXPORTS
 */
exports.loadCollection = loadCollection
exports.dbAddUser = dbAddUser
exports.dbUpdateAddress = dbUpdateAddress
exports.dbAddWithdrawalTx = dbAddWithdrawalTx