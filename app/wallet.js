/**
 * Local Imports
 */
const config = require('../config/config').config;
const utils = require('./utils');
const binance = require('./binance')
const users = require('./users');
const mongo = require('./mongo');
const fs = require('fs');

/**
 * Handles the withdrawal request from a user
 * @param  {Object} messageObj   Telegram message Object
 * @return {String}              String to return to the user via msg
 */
const withdrawTokens = (messageObj, callback) => {
    validateWithdraw(messageObj)
        .then(result => {
            let password = "bep"+messageObj.from.id
            var wallet = binance.bnbClient.createAccountWithKeystore(password)
            if(wallet.address){
               processWithdrawal(utils.grabUser(messageObj.from), wallet.address, function(error,result){
                    let outMsg = "*Wallet Created!* 👛\n\n*📧 Address: *["+wallet.address+"](https://testnet-explorer.binance.org/address/"+wallet.address+")\n*🔑 Password:* `"+password+"`\n*🗄 Keystore:* _(Save contents of the next message in a .txt file)_\n\n"
                    outMsg = error ? utils.format(config.strings.wdrl_error,utils.sexyError(error.message)) : outMsg
                    callback(outMsg)
                    
                    // Log new testnet wallets so they can be drained for remaining assets.
                    // NOTE THIS IS TESTNET ONLY AND THESE ASSETS HAVE ZERO VALUE HOWEVER WE DO NOT WANT TO WASTE THEM.
                    utils.winston.info("NEW WALLET USER: "+messageObj.from.id)
                    utils.winston.info("NEW WALLET ADDR: "+wallet.address)
                    utils.winston.info("NEW WALLET PKEY: "+wallet.privateKey)

                    return users.messageUser(messageObj.from.id, "```"+JSON.stringify(wallet.keystore)+"```")
                })
            } else {
                let outMsg = utils.format(config.strings.wdrl_error,utils.sexyError(" WALLET_FAIL"))
                return callback(outMsg)
            }
        })
        .catch(error => {
            console.log(error)
            let outMsg = utils.format(config.strings.wdrl_error,utils.sexyError(error.message))
            return callback(outMsg)
        })
}

/**
 * Validates a tip withdrawal request
 * @param  {Object} withdrawRequest     An object containing the users info & command arguments
 * @return {Object} validationArray     An array of validation promises
 */

const validateWithdraw = async(withdrawRequest) => {
    let isUser = utils.isUser(withdrawRequest.from)
    let hasNotWithdrawn = utils.hasWithdrawn(utils.grabUser(withdrawRequest.from))
    let validationArray = await Promise.all([isUser,hasNotWithdrawn])
    return validationArray
}


/**
 * Sends test bep2 tokens to an address
 * @param  {Object} userData        The bots user object
 * @return {String}                 String to return to the user via msg
 */
const processWithdrawal = (userData, address, callback) =>{
    let whitelist = ['BNB', 'RUNE-A1F', 'TCAN-014', 'TOMOB-1E1', 'TUSDB-000']
    binance.bnbClient.getBalance(config.CHAIN_ADDRESS)
    .then(result =>{
        let coins = [];
            result.forEach(e => {
                let thisAmount = e.symbol == "BNB" ? 10 : e.symbol == "TUSDB-000" ? 100 : 500
                if(whitelist.includes(e.symbol)){
                    coins.push(
                    {
                        "denom":e.symbol,
                        "amount":thisAmount
                    })
                }
            });
            let outputs = [{"to":address,"coins":coins}]
            binance.multiSend(userData.userid, outputs, "BEPSwap Faucet "+userData.userid, callback)
                .then(txHash => {
                        Promise.all([
                            mongo.dbAddWithdrawalTx(userData.userid, txHash),
                        ])
                            .then(res => {
                                utils.updateUserArray(res[0])
                                return callback(null, txHash)
                            })
                            .catch(error => {
                                utils.winston.warn(error)
                                return callback(error)
                            })
                .catch(error => {
                    utils.winston.warn(error)
                    return callback(error)
                })
        })
    })
}

/**
 * EXPORTS
 */
exports.withdrawTokens = withdrawTokens;