
const config = {
    CHAIN_WSURI:    'wss://testnet-dex.binance.org/api/ws/',
    CHAIN_APIURI:   'https://testnet-dex.binance.org/',
    CHAIN_ADDRESS:  process.env.CHAIN_ADDRESS,
    CHAIN_PRIVKEY:  process.env.CHAIN_PRIVKEY,
    CHAIN_QRCODE:   process.env.CHAIN_QRCODE,
    EXPRESS_PORT:   process.env.PORT,
    MONGO_URL:      process.env.MONGO_URL,
    MONGO_DBNAME:   process.env.MONGO_DBNAME,
    TELEGRAM_TOKEN: process.env.TELEGRAM_TOKEN,
    strings: {
            "welcome": "*ABOUT BEPSWAP BOT* ⚡️⚡️⚡️\n\nThe *BEPSwap Bot* is presently a Binance Chain testnet faucet (until BEPSwap launch).\n\n*1️⃣ STEP 1:\n* Select *💰 Get Wallet* from the keyboard menu or type /getwallet. You will receive details for a keystore which comes preloaded with testnet BEP2 tokens.\n\n*2️⃣ STEP 2:\n* Save the keystore text to a .txt file and note your public address & password.\n\n*3️⃣ STEP 3:\n* Head on over to https://testnet.bepswap.com and unlock your wallet using the keystore above!\n\n*Happy testing!* 🧪⚗️\n\n",
            "start": [
                ["⚡️ Bot Info"],
                ["💰 Get Wallet"]
            ],
            "about": "*ABOUT BEPSWAP BOT* ⚡️⚡️⚡️\n\nThe *BEPSwap Bot* is presently a Binance Chain testnet faucet (until BEPSwap launch).\n\n*1️⃣ STEP 1:\n* Select *💰 Get Wallet* from the keyboard menu or type /getwallet. You will receive details for a keystore which comes preloaded with testnet BEP2 tokens.\n\n* 2️⃣STEP 2:\n* Save the keystore text to a .txt file and note your public address & password.\n\n*3️⃣ STEP 3:\n* Head on over to https://testnet.bepswap.com and unlock your wallet using the keystore above!\n\n*Happy testing!* 🧪⚗️\n\n",
            "wdrl_pending": "*💰 WALLET REQUESTED *\n\n⏳ Hold tight while we run some checks.",
            "wdrl_error": "*💰 WALLET ERROR*\n\n{0}\n\n",
            "wdrl_success": "*WITHDRAWAL SUCCESSFUL!* 💰💰💰\n\n📤*Transaction Hash:* [{0}](https://testnet-explorer.binance.org/tx/{0}",
    }
}

exports.config = config;